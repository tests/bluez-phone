#!/bin/sh

set -eu

PAIRADDR=""
SKIP_PAIRING="0"

fail_exit() {
	echo "FAILED"
}

usage() {
	echo "Usage: $0 -a <bdaddr> [-x]"
	exit 1
}

while [ $# -gt 0 ] ; do
	case $1 in
	"-a") shift; PAIRADDR=$1 ;;
	"-x") set -x ;;
	"-s") SKIP_PAIRING="1" ;;
	*) usage
	esac
	shift
done

alias grep='grep -q'

OBJECT_MANAGER="gdbus call --system --dest org.bluez --object-path / --method org.freedesktop.DBus.ObjectManager"

AGENT_MANAGER="gdbus call --system --dest org.bluez --object-path /org/bluez --method org.bluez.AgentManager1"

MANAGER="gdbus call --system --dest org.bluez --object-path /org/bluez --method org.bluez.Manager1"

strip_quote() {
	echo $* | sed -e "s/.*'\(.*\)'.*/\1/"
}

select_adapter() {
	echo select_adapter
	ADAPTER_PATH=""
	OBJECTS=`${OBJECT_MANAGER}.GetManagedObjects`
	for WORD in ${OBJECTS} ; do
		case ${WORD} in
		"'/org/bluez/hci"[0-9]"':")
			ADAPTER_PATH=`strip_quote ${WORD}`
			break
			;;
		esac
	done

	echo "Selected ${ADAPTER_PATH}"
	ADAPTER="gdbus call --system --dest org.bluez --object-path ${ADAPTER_PATH} --method org.bluez.Adapter1"
	ADAPTER_IFACE=${ADAPTER_PATH#/org/bluez/}
}

select_device() {
	echo "select_device: Discovering..."
	bluez-phone-scanner &
	SCANPID=$!
	sleep 10
	kill $SCANPID

	local OBJECTS
	OBJECTS=`${OBJECT_MANAGER}.GetManagedObjects`
	for WORD in ${OBJECTS} ; do
		case ${WORD} in
		"'${ADAPTER_PATH}/dev_"*"':")
			local DEVICE_PATH
			DEVICE_PATH=`strip_quote ${WORD}`
			local DEVICEPROPERTIES
			DEVICEPROPERTIES="gdbus call --system --dest org.bluez --object-path ${DEVICE_PATH} --method org.freedesktop.DBus.Properties"
			local BDADDR
			BDADDR=`${DEVICEPROPERTIES}.Get "org.bluez.Device1" "Address"`
			BDADDR=`strip_quote ${BDADDR}`
			local BTNAME
			BTNAME=`${DEVICEPROPERTIES}.Get "org.bluez.Device1" "Name" || echo "<unnamed>"`
			BTNAME=`strip_quote ${BTNAME}`
			echo "Device found: ${BDADDR} ${BTNAME}"
			;;
		esac
	done

	echo -n "Input device address: "
	read SELECTION
	echo "Selected address: $SELECTION"

	PAIRADDR=${SELECTION}
}

pair_device_initiator() {
	local BDADDR
	BDADDR=$1
	pair_two hci0 ${BDADDR}
}

pair_device_responder() {
	local BDADDR
	BDADDR=$1

	echo "Start a pairing from the phone ${BDADDR}! "
	pair_two hci0 ${BDADDR} -l
}

check_device() {
	local BDADDR
	BDADDR=`echo $1 | sed "s/:/_/g"`

	local OBJECTS
	OBJECTS=`${OBJECT_MANAGER}.GetManagedObjects`
	DEVICE_PATH=""
	for WORD in ${OBJECTS} ; do
		case ${WORD} in
		"'${ADAPTER_PATH}/dev_${BDADDR}':")
			DEVICE_PATH=`strip_quote ${WORD}`
			echo "Device found: ${DEVICE_PATH}"
			;;
		esac
	done

	if [ -n "${DEVICE_PATH}" ] ; then
		DEVICE="gdbus call --system --dest org.bluez --object-path ${DEVICE_PATH} --method org.bluez.Device1"
		DEVICEPROPERTIES="gdbus call --system --dest org.bluez --object-path ${DEVICE_PATH} --method org.freedesktop.DBus.Properties"
		${DEVICEPROPERTIES}.Get "org.bluez.Device1" "Paired" | grep "(<true>,)"
		DEVICE_BDADDR=`${DEVICEPROPERTIES}.Get "org.bluez.Device1" "Address"`
		DEVICE_BDADDR=`strip_quote ${DEVICE_BDADDR}`
		echo "Device ${BDADDR} is paired"
	else
		echo "Device ${BDADDR} is not paired"
		exit 1
	fi
}

test_pairing_initiator() {
	echo test_pairing_initiator
	local BDADDR
	BDADDR=$1

	pair_device_initiator ${BDADDR}
	check_device ${BDADDR}
}

test_pairing_responder() {
	echo test_pairing_responder
	local BDADDR
	BDADDR=$1

	pair_device_responder ${BDADDR}
	check_device ${BDADDR}
}


create_obex_session() {
	local PROFILE
	PROFILE=$1
	local INTERFACE
	INTERFACE=$2

	# Do not create the session using gdbus because bluez will release it as soon as the process terminates
	# Instead start a helper process in background to keep the obex session opened
	#local CLIENT="gdbus call --session --dest org.bluez.obex --object-path /org/bluez/obex --method org.bluez.obex.Client1"
	#local SESSION_PATH=`${CLIENT}.CreateSession ${DEVICE_BDADDR} "{ 'Target': <'pbap'> }" | sed -e "s/.*'\(.*\)'.*/\1/"`
	kill -9 $(pidof bluez-phone-tester) >/dev/null 2>&1 || true
	OBEXFIFOOUT=out
	rm -f ${OBEXFIFOOUT}
	mkfifo ${OBEXFIFOOUT}

	bluez-phone-tester ${DEVICE_BDADDR} ${PROFILE} >${OBEXFIFOOUT} &

	SESSION_PATH=`timeout 30 head -1 ${OBEXFIFOOUT} || true`
	SESSION_PATH=`strip_quote ${SESSION_PATH}`

	[ -z "${SESSION_PATH}" ] && echo "Invalid session for ${PROFILE} ${INTERFACE}" && exit 1

	SESSION_PATH="gdbus call --session --dest org.bluez.obex --object-path ${SESSION_PATH} --method org.bluez.obex.${INTERFACE}"
}

test_profile_opp_client() {
	echo test_profile_opp_client
	create_obex_session opp ObjectPush1
	OPP=${SESSION_PATH}

cat > /tmp/vcard.vcf <<EOF
BEGIN:VCARD
VERSION:2.1
N:;Collabora;;;
FN:Collabora
TEL;CELL;PREF:145
END:VCARD
EOF

	echo "Transfer will start"
	${OPP}.SendFile "/tmp/vcard.vcf"
	echo "Please ensure the transfer is received on the target, was it received? (y/n)"
	read ANSWER
	if [ "${ANSWER}" != "y" ] ; then
		echo "OPP Server test failed"
		exit 1
	fi
}

test_profile_opp_server() {
	echo test_profile_opp_server

	kill $(pidof bluez-phone-obex-agent) >/dev/null 2>&1 || true
	bluez-phone-obex-agent &OBEX_AGENT_PID=$!

	OBEX_AGENT_MANAGER="gdbus call --session --dest org.bluez.obex --object-path /org/bluez/obex --method org.bluez.obex.AgentManager1"

	echo "Start an image/video file transfer in the phone, was it successful? (y/n)"
	read ANSWER

	kill ${OBEX_AGENT_PID} ||:

	if [ "${ANSWER}" != "y" ] ; then
		echo "OPP Server test failed"
		exit 1
	fi
}

test_profiles() {
	echo test_profiles

	UUIDS=`${DEVICEPROPERTIES}.Get "org.bluez.Device1" "UUIDs"`
	for WORD in ${UUIDS} ; do
		case ${WORD} in
		*"00001105-0000-1000-8000-00805f9b34fb"*) echo "OBEXObjectPush"                  ; test_profile_opp_server;;
		*) echo "Unknown profile ${WORD}" ;;
		esac
	done

	test_profile_opp_client
}

trap fail_exit EXIT

. common/update-test-path
select_adapter

if [ -z "${PAIRADDR}" ] ; then
	select_device
fi

if [ "${SKIP_PAIRING}" = "0" ] ; then
	test_pairing_initiator ${PAIRADDR}
	test_pairing_responder ${PAIRADDR}
else
	check_device ${PAIRADDR}
fi

test_profiles ${PAIRADDR}

trap '' EXIT
echo "PASSED"
