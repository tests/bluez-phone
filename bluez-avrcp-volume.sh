#!/bin/sh

set -eu

PAIRADDR=""
SKIP_PAIRING="0"

fail_exit() {
	echo "FAILED"
}

usage() {
	echo "Usage: $0 -a <bdaddr> [-x]"
	exit 1
}

while [ $# -gt 0 ] ; do
	case $1 in
	"-a") shift; PAIRADDR=$1 ;;
	"-x") set -x ;;
	"-s") SKIP_PAIRING="1" ;;
	*) usage
	esac
	shift
done

OBJECT_MANAGER="gdbus call --system --dest org.bluez --object-path / --method org.freedesktop.DBus.ObjectManager"

strip_quote() {
	echo $* | sed -e "s/.*'\(.*\)'.*/\1/"
}

select_adapter() {
	echo "select_adapter"
	ADAPTER_PATH=""
	OBJECTS=$(${OBJECT_MANAGER}.GetManagedObjects)
	for WORD in ${OBJECTS} ; do
		case ${WORD} in
		"'/org/bluez/hci"[0-9]"':")
			ADAPTER_PATH=$(strip_quote ${WORD})
			break
			;;
		esac
	done
        if [ -z "${ADAPTER_PATH}" ]; then
            echo "Hci device not found"
            exit 1
        fi
        echo "Selected ${ADAPTER_PATH}"
}

select_device() {
	echo "select_device: Discovering..."
	bluez-phone-scanner &
	SCANPID=$!
	sleep 10
	kill $SCANPID

	local OBJECTS
	OBJECTS=$(${OBJECT_MANAGER}.GetManagedObjects)
	for WORD in ${OBJECTS} ; do
		case ${WORD} in
		"'${ADAPTER_PATH}/dev_"*"':")
			local DEVICE_PATH
			DEVICE_PATH=$(strip_quote ${WORD})
			local DEVICEPROPERTIES
			DEVICEPROPERTIES="gdbus call --system --dest org.bluez --object-path ${DEVICE_PATH} --method org.freedesktop.DBus.Properties"
			local BDADDR
			BDADDR=$(${DEVICEPROPERTIES}.Get "org.bluez.Device1" "Address")
			BDADDR=$(strip_quote ${BDADDR})
			local BTNAME
			BTNAME=$(${DEVICEPROPERTIES}.Get "org.bluez.Device1" "Name" || echo "<unnamed>")
			BTNAME=$(strip_quote ${BTNAME})
			echo "Device found: ${BDADDR} ${BTNAME}"
			;;
		esac
	done

	echo -n "Input device address: "
	read SELECTION
	echo "Selected address: $SELECTION"

	PAIRADDR=${SELECTION}
}

pair_device_initiator() {
	local BDADDR
	BDADDR=$1
	pair_two hci0 ${BDADDR}
}

pair_device_responder() {
	local BDADDR
	BDADDR=$1

	echo "Start a pairing from the phone ${BDADDR}! "
	pair_two hci0 ${BDADDR} -l
}

check_device() {
	local BDADDR
	BDADDR=$(echo $1 | sed "s/:/_/g")

	local OBJECTS
	OBJECTS=$(${OBJECT_MANAGER}.GetManagedObjects)
	DEVICE_PATH=""
	for WORD in ${OBJECTS} ; do
		case ${WORD} in
		"'${ADAPTER_PATH}/dev_${BDADDR}':")
			DEVICE_PATH=$(strip_quote ${WORD})
			echo "Device found: ${DEVICE_PATH}"
			;;
		esac
	done

	if [ -n "${DEVICE_PATH}" ] ; then
		DEVICE="gdbus call --system --dest org.bluez --object-path ${DEVICE_PATH} --method org.bluez.Device1"
		DEVICEPROPERTIES="gdbus call --system --dest org.bluez --object-path ${DEVICE_PATH} --method org.freedesktop.DBus.Properties"
		value_check=$(${DEVICEPROPERTIES}.Get "org.bluez.Device1" "Paired")
                echo "Value checked is ${value_check}"
                if [ ${value_check} != "(<true>,)" ]; then
                    echo "Not paired"
                    exit 1
                fi
		DEVICE_BDADDR=$(${DEVICEPROPERTIES}.Get "org.bluez.Device1" "Address")
		DEVICE_BDADDR=$(strip_quote ${DEVICE_BDADDR})
		echo "Device ${BDADDR} is paired"
	else
		echo "Device ${BDADDR} is not paired"
		exit 1
	fi
}

bluez_avrcp_volume() {
    echo "connecting to the device"
    ${DEVICE}.Connect
    sleep 5
    CONTROL="gdbus call --system --dest org.bluez --object-path ${DEVICE_PATH} --method org.bluez.MediaControl1"
    echo "Volume Down" 
    ${CONTROL}.VolumeDown
    echo "Volume Up"
    ${CONTROL}.VolumeUp
	
}
test_pairing_initiator() {
	echo "test_pairing_initiator"
	local BDADDR
	BDADDR=$1

	pair_device_initiator ${BDADDR}
	check_device ${BDADDR}
}

test_pairing_responder() {
	echo "test_pairing_responder"
	local BDADDR
	BDADDR=$1

	pair_device_responder ${BDADDR}
	check_device ${BDADDR}
}

trap fail_exit EXIT

. common/update-test-path
select_adapter

if [ -z "${PAIRADDR}" ] ; then
	select_device
fi

if [ "${SKIP_PAIRING}" = "0" ] ; then
	test_pairing_initiator ${PAIRADDR}
	test_pairing_responder ${PAIRADDR}
else
	check_device ${PAIRADDR}
fi

bluez_avrcp_volume

trap '' EXIT
echo "PASSED"
