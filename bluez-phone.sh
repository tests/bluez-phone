#!/bin/sh

set -eu

PAIRADDR=""
SKIP_PAIRING="0"

fail_exit() {
	echo "FAILED"
}

usage() {
	echo "Usage: $0 -a <bdaddr> [-x]"
	exit 1
}

while [ $# -gt 0 ] ; do
	case $1 in
	"-a") shift; PAIRADDR=$1 ;;
	"-x") set -x ;;
	"-s") SKIP_PAIRING="1" ;;
	*) usage
	esac
	shift
done

alias grep='grep -q'

OBJECT_MANAGER="gdbus call --system --dest org.bluez --object-path / --method org.freedesktop.DBus.ObjectManager"

AGENT_MANAGER="gdbus call --system --dest org.bluez --object-path /org/bluez --method org.bluez.AgentManager1"

MANAGER="gdbus call --system --dest org.bluez --object-path /org/bluez --method org.bluez.Manager1"

strip_quote() {
	echo $* | sed -e "s/.*'\(.*\)'.*/\1/"
}

select_adapter() {
	echo select_adapter
	ADAPTER_PATH=""
	OBJECTS=`${OBJECT_MANAGER}.GetManagedObjects`
	for WORD in ${OBJECTS} ; do
		case ${WORD} in
		"'/org/bluez/hci"[0-9]"':")
			ADAPTER_PATH=`strip_quote ${WORD}`
			break
			;;
		esac
	done

	echo "Selected ${ADAPTER_PATH}"
	ADAPTER="gdbus call --system --dest org.bluez --object-path ${ADAPTER_PATH} --method org.bluez.Adapter1"
	ADAPTER_IFACE=${ADAPTER_PATH#/org/bluez/}
}

select_device() {
	echo "select_device: Discovering..."
	bluez-phone-scanner &
	SCANPID=$!
	sleep 10
	kill $SCANPID

	local OBJECTS
	OBJECTS=`${OBJECT_MANAGER}.GetManagedObjects`
	for WORD in ${OBJECTS} ; do
		case ${WORD} in
		"'${ADAPTER_PATH}/dev_"*"':")
			local DEVICE_PATH
			DEVICE_PATH=`strip_quote ${WORD}`
			local DEVICEPROPERTIES
			DEVICEPROPERTIES="gdbus call --system --dest org.bluez --object-path ${DEVICE_PATH} --method org.freedesktop.DBus.Properties"
			local BDADDR
			BDADDR=`${DEVICEPROPERTIES}.Get "org.bluez.Device1" "Address"`
			BDADDR=`strip_quote ${BDADDR}`
			local BTNAME
			BTNAME=`${DEVICEPROPERTIES}.Get "org.bluez.Device1" "Name" || echo "<unnamed>"`
			BTNAME=`strip_quote ${BTNAME}`
			echo "Device found: ${BDADDR} ${BTNAME}"
			;;
		esac
	done

	echo -n "Input device address: "
	read SELECTION
	echo "Selected address: $SELECTION"

	PAIRADDR=${SELECTION}
}

pair_device_initiator() {
	local BDADDR
	BDADDR=$1
	pair_two hci0 ${BDADDR}
}

pair_device_responder() {
	local BDADDR
	BDADDR=$1

	echo "Start a pairing from the phone ${BDADDR}! "
	pair_two hci0 ${BDADDR} -l
}

check_device() {
	local BDADDR
	BDADDR=`echo $1 | sed "s/:/_/g"`

	local OBJECTS
	OBJECTS=`${OBJECT_MANAGER}.GetManagedObjects`
	DEVICE_PATH=""
	for WORD in ${OBJECTS} ; do
		case ${WORD} in
		"'${ADAPTER_PATH}/dev_${BDADDR}':")
			DEVICE_PATH=`strip_quote ${WORD}`
			echo "Device found: ${DEVICE_PATH}"
			;;
		esac
	done

	if [ -n "${DEVICE_PATH}" ] ; then
		DEVICE="gdbus call --system --dest org.bluez --object-path ${DEVICE_PATH} --method org.bluez.Device1"
		DEVICEPROPERTIES="gdbus call --system --dest org.bluez --object-path ${DEVICE_PATH} --method org.freedesktop.DBus.Properties"
		${DEVICEPROPERTIES}.Get "org.bluez.Device1" "Paired" | grep "(<true>,)"
		DEVICE_BDADDR=`${DEVICEPROPERTIES}.Get "org.bluez.Device1" "Address"`
		DEVICE_BDADDR=`strip_quote ${DEVICE_BDADDR}`
		echo "Device ${BDADDR} is paired"
	else
		echo "Device ${BDADDR} is not paired"
		exit 1
	fi
}

test_pairing_initiator() {
	echo test_pairing_initiator
	local BDADDR
	BDADDR=$1

	pair_device_initiator ${BDADDR}
	check_device ${BDADDR}
}

test_pairing_responder() {
	echo test_pairing_responder
	local BDADDR
	BDADDR=$1

	pair_device_responder ${BDADDR}
	check_device ${BDADDR}
}

test_profile_nap() {
	echo test_profile_nap

	MANAGER="gdbus call --system --dest net.connman --object-path / --method net.connman.Manager"

	SERVICES=`${MANAGER}.GetServices`
	for WORD in ${SERVICES} ; do
		case ${WORD} in
		"'/net/connman/service/"*)
			OBJECT_PATH=`strip_quote ${WORD}`
			SERVICE="gdbus call --system --dest net.connman --object-path ${OBJECT_PATH} --method net.connman.Service"

			NAME=`${SERVICE}.GetProperties | grep "Name"`

			echo "NAP:service name ${NAME}"

			if [ "${NAME}" = "${DEVICE_BDADDR}" ] ; then
				${SERVICE}.Connect --timeout 60
				wget http://connman.net
				${SERVICE}.Disconnect --timeout 60
			fi
			;;
		esac
	done

        #manager = dbus.Interface(bus.get_object("net.connman", "/"),
                                 #"net.connman.Manager")
        #services = manager.GetServices()

        #device_name = self._device_obj.Get(
            #'org.bluez.Device1',
            #'Name',
            #dbus_interface='org.freedesktop.DBus.Properties')

        #path = None
        #for s in services:
            #if s[1]["Name"] == device_name:
                #path = s[0]
                #break
        #if not path:
            #raise LookupError('Device ‘%s’ not found in services' % device_name)

        #service = dbus.Interface(bus.get_object("net.connman", path),
                                 #"net.connman.Service")
        #service.Connect(timeout=60000)
        #url = urllib.request.urlopen("http://connman.net")
        #f = tempfile.TemporaryFile('w')
        #f.write(url.read(1000000).decode('utf-8'))
        #f.close()
        #service.Disconnect(timeout=60000)

}

create_obex_session() {
	local PROFILE
	PROFILE=$1
	local INTERFACE
	INTERFACE=$2

	# Do not create the session using gdbus because bluez will release it as soon as the process terminates
	# Instead start a helper process in background to keep the obex session opened
	#local CLIENT="gdbus call --session --dest org.bluez.obex --object-path /org/bluez/obex --method org.bluez.obex.Client1"
	#local SESSION_PATH=`${CLIENT}.CreateSession ${DEVICE_BDADDR} "{ 'Target': <'pbap'> }" | sed -e "s/.*'\(.*\)'.*/\1/"`
	kill -9 $(pidof bluez-phone-tester) >/dev/null 2>&1 || true
	OBEXFIFOOUT=out
	rm -f ${OBEXFIFOOUT}
	mkfifo ${OBEXFIFOOUT}

	bluez-phone-tester ${DEVICE_BDADDR} ${PROFILE} >${OBEXFIFOOUT} &

	SESSION_PATH=`timeout 30 head -1 ${OBEXFIFOOUT} || true`
	SESSION_PATH=`strip_quote ${SESSION_PATH}`

	[ -z "${SESSION_PATH}" ] && echo "Invalid session for ${PROFILE} ${INTERFACE}" && exit 1

	SESSION_PATH="gdbus call --session --dest org.bluez.obex --object-path ${SESSION_PATH} --method org.bluez.obex.${INTERFACE}"
}

test_profile_map_mse() {
	echo test_profile_map_mse
	create_obex_session map MessageAccess1
	MAP=${SESSION_PATH}
	${MAP}.SetFolder "telecom/msg"
	FOLDERS=`${MAP}.ListFolders "{}"`
	for WORD in ${FOLDERS} ; do
		local FOLDER
		FOLDER=`strip_quote ${WORD}`
		if [ "${FOLDER}" != "Name" ]; then
			local CHECK
			CHECK="msteryia"
			# For remote phone with a lot of messages, retrieving all messages in a folder can last longer than the default DBus reply timeout
			# Retrieving only the first 50 messages from each box is sufficient for this test
			MESSAGES=`${MAP}.ListMessages "${FOLDER}" "{'Offset': <uint16 0>, 'MaxCount': <uint16 50>}"`

			for MESSAGE in ${MESSAGES} ; do
				case ${MESSAGE} in
				*"/org/bluez/"*)
					if [ "msteryia" = "${CHECK}" ] ; then
						CHECK="m"
					else
						echo "Invalid parameters in MAP message test: ${CHECK}"
						exit 1
					fi ;;
				*"'Subject'"*) CHECK="${CHECK}s";;
				*"'Timestamp'"*) CHECK="${CHECK}t";;
				*"'SenderAddress'"*) CHECK="${CHECK}e";;
				*"'RecipientAddress'"*) CHECK="${CHECK}r";;
				*"'Type'"*) CHECK="${CHECK}y";;
				*"'Size'"*) CHECK="${CHECK}i";;
				*"'Status'"*) CHECK="${CHECK}a";;
				esac
			done
		fi
	done
}

test_profile_opp_client() {
	echo test_profile_opp_client
	create_obex_session opp ObjectPush1
	OPP=${SESSION_PATH}

cat > /tmp/vcard.vcf <<EOF
BEGIN:VCARD
VERSION:2.1
N:;Collabora;;;
FN:Collabora
TEL;CELL;PREF:145
END:VCARD
EOF

	echo "Transfer will start"
	${OPP}.SendFile "/tmp/vcard.vcf"
	echo "Please ensure the transfer is received on the target, was it received? (y/n)"
	read ANSWER
	if [ "${ANSWER}" != "y" ] ; then
		echo "OPP Server test failed"
		exit 1
	fi
}

test_profile_opp_server() {
	echo test_profile_opp_server

	kill $(pidof bluez-phone-obex-agent) >/dev/null 2>&1 || true
	bluez-phone-obex-agent &OBEX_AGENT_PID=$!

	OBEX_AGENT_MANAGER="gdbus call --session --dest org.bluez.obex --object-path /org/bluez/obex --method org.bluez.obex.AgentManager1"

	echo "Start an OPP transfer in the phone, was it successful? (y/n)"
	read ANSWER

	kill ${OBEX_AGENT_PID} ||:

	if [ "${ANSWER}" != "y" ] ; then
		echo "OPP Server test failed"
		exit 1
	fi
}

test_profile_a2dp_src() {
	echo test_profile_a2dp_src

	DEVICE="gdbus call --system --dest org.bluez --object-path ${DEVICE_PATH} --method org.bluez.Device1"
	DEVICE_CONNECTED=`${DEVICEPROPERTIES}.Get "org.bluez.Device1" "Connected"`
	DEVICE_CONNECTED=`strip_quote ${DEVICE_CONNECTED}`
	echo "Connecting"
	if echo "${DEVICE_CONNECTED}" | grep -q "false" ; then
		${DEVICE}.Connect > /dev/null
	fi

	echo "Play music from the device! Did you hear it (y/n): "
	read ANSWER
	if [ "${ANSWER}" != "y" ] ; then
		echo "A2DP test failed"
		exit 1
	fi
	echo "Disconnecting"
	${DEVICE}.Disconnect  > /dev/null
}

test_profile_pbap_pse() {
	echo test_profile_pbap_pse
	create_obex_session pbap PhonebookAccess1
	PBAP=${SESSION_PATH}

	for PBAP_PATH in "PB" "ICH" "OCH" "MCH" "CCH"; do
		echo "${PBAP_PATH}"
		${PBAP}.Select "int" "${PBAP_PATH}" > /dev/null
		echo "Number of contacts: "`${PBAP}.GetSize`
		VCARDS=`${PBAP}.List "{}"`
		for WORD in ${VCARDS} ; do
			WORD=`strip_quote ${WORD}`
			case WORD in
			"*.vcf") ${PBAP}.Pull "${WORD}" "" "{}"  > /dev/null ;;
			esac
		done
	done
}

test_profiles() {
	echo test_profiles

	UUIDS=`${DEVICEPROPERTIES}.Get "org.bluez.Device1" "UUIDs"`
	for WORD in ${UUIDS} ; do
		case ${WORD} in
		*"00001104-0000-1000-8000-00805f9b34fb"*) echo "IrMCSync" ;;
		*"00001105-0000-1000-8000-00805f9b34fb"*) echo "OBEXObjectPush"                  ; test_profile_opp_server;;
		*"00001106-0000-1000-8000-00805f9b34fb"*) echo "OBEXFileTransfer" ;;
		*"0000110a-0000-1000-8000-00805f9b34fb"*) echo "AudioSource"                     ; test_profile_a2dp_src ;;
		*"0000110b-0000-1000-8000-00805f9b34fb"*) echo "AudioSink" ;;
		*"0000110c-0000-1000-8000-00805f9b34fb"*) echo "AV Remote Control Target" ;;
		*"0000110e-0000-1000-8000-00805f9b34fb"*) echo "AV Remote Control" ;;
		*"00001112-0000-1000-8000-00805f9b34fb"*) echo "Headset Audio Gateway" ;;
		*"00001115-0000-1000-8000-00805f9b34fb"*) echo "PANU" ;;
		*"00001116-0000-1000-8000-00805f9b34fb"*) echo "NAP"                             ; test_profile_nap ;;
		*"0000111e-0000-1000-8000-00805f9b34fb"*) echo "Handsfree" ;;
		*"0000111f-0000-1000-8000-00805f9b34fb"*) echo "Handsfree Audio Gateway" ;;
		*"0000112d-0000-1000-8000-00805f9b34fb"*) echo "SimAccess" ;;
		*"0000112f-0000-1000-8000-00805f9b34fb"*) echo "PBAP Phonebook Access PSE"       ; test_profile_pbap_pse ;;
		*"00001132-0000-1000-8000-00805f9b34fb"*) echo "MAP Message Access Server"       ; test_profile_map_mse ;;
		*"00001133-0000-1000-8000-00805f9b34fb"*) echo "MAP Message Notification Server" ;;
		*"00001200-0000-1000-8000-00805f9b34fb"*) echo "PnPInformation" ;;
		*"00005005-0000-1000-8000-0002ee000001"*) echo "Nokia PC Suite OBEX UUID" ;;
		*) echo "Unknown profile ${WORD}" ;;
		esac
	done

	test_profile_opp_client
}

trap fail_exit EXIT

. common/update-test-path
select_adapter

if [ -z "${PAIRADDR}" ] ; then
	select_device
fi

if [ "${SKIP_PAIRING}" = "0" ] ; then
	test_pairing_initiator ${PAIRADDR}
	test_pairing_responder ${PAIRADDR}
else
	check_device ${PAIRADDR}
fi

test_profiles ${PAIRADDR}

trap '' EXIT
echo "PASSED"
